# ebi-error-pages

The purpose of this repository is to store common error pages that appear on EBI domains or related services. Any K8s project that runs EBI theme service can use this repository as a submodule and point their service error pages to this repository's error pages.

## Project owners

[EMBL-EBI Web Development Team](https://www.ebi.ac.uk/about/teams/its-web-development/)

## Getting started

Clone this repository as submodule in their repository with below command.

```
git submodule add --force https://gitlab.ebi.ac.uk/ebiwd/ebi-error-pages.git error-pages
```

### Updating error pages from main repo

Run below command if you would like to update submodule in your repository

```
git submodule foreach --recursive git fetch
git submodule foreach git merge origin master
```

### Usage example

Example of how you can configure error pages in k8s if you are using Nginx.

```
server {
    listen              8080;
    server_name         localhost;
    absolute_redirect   off;
    root                /usr/share/nginx/html;
    index               index.html;

    location / {
      expires         5m;
      add_header      Cache-Control "public";
      try_files $uri $uri/ /index.html;
    }

    error_page 404 /404/index.html;
    location = /404/index.html {
      root /usr/share/nginx/html/error-pages;
      internal;
    }

    error_page 403 /403/index.html;
    location = /403/index.html {
      root /usr/share/nginx/html/error-pages;
      internal;
    }

    # redirect server error pages to the static page /50x.html
    #
    error_page 500 502 503 504  /500/index.html;
    location = /500/index.html {
      root /usr/share/nginx/html/error-pages;
    }
}
```

## Updates/changes

EMBL-EBI Web Development will regularly update these pages whenever there is any change in theme or structure.


## Reporting issues

Please raise ticket in [ServiceNow](https://embl.service-now.com/esc).
